#!/usr/bin/env bash

set -e

provision() {
  echo  "==> Provisioning: ${1} module"
  make init -upgrade
  make validate
  ([[ -n "$( terraform state list )" ]] && make destroy apply) || true # ignore failures
  make auto
  make destroy apply || true # ignore failures
}

validate() {
  echo  "==> Validating: ${1} module"
  terraform init -backend=false -upgrade
  terraform validate
}

build() {
  echo "==> Building: ${example}"
  cd "${1}"
  [[ ! -e Makefile ]] && validate "${example}"
  [[ -e Makefile ]] && provision "${example}"
  cd -
}


validate "root"

for example in examples/*; do
  build "${example}"
done
